import QtQuick 2.7
import Ubuntu.Components 1.3

import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import Ubuntu.Components.Popups 1.3

import "components"

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'qturn.cibersheep'
    automaticOrientation: true

    signal pieceChanged(int whichPiece, string whichProperty, int whichValue)
    property bool debugMode: false

    property var   board: []
    property var player1: {}
    property var player2: {}
    property bool isLandscape: width > height - units.gu(6) //header

    Settings {
        id: generalSettings
        property bool firstAppRun: true
    }

    width: units.gu(45)
    height: units.gu(75)

    PageStack {
        id: mainStack
        anchors.fill: parent
    }

    Component.onCompleted: {
        initPlayers();
        initBoard();
        mainStack.push(Qt.resolvedUrl("components/PageBoard.qml"));
        if (generalSettings.firstAppRun) {
            PopupUtils.open(readRules);
        }
    }

    Component {
        id: readRules

        Dialog {
            title: i18n.tr("Read Game Rules")

            Text {
                text: i18n.tr("This is the first time you play. Do you want to read the game rules?")
                wrapMode: Text.Wrap
            }

            Button {
                text: i18n.tr("Read rules")
                color: theme.palette.normal.activity

                onClicked: {
                    setFirstRun();
                    hide();
                    mainStack.push(Qt.resolvedUrl("components/PageAbout.qml"));
                }
            }

            Button {
                text: i18n.tr("Start Playing")

                onClicked: {
                    setFirstRun();
                    hide();
                }
            }

            function setFirstRun() {
                generalSettings.firstAppRun = false;
            }
        }
    }

    onPieceChanged: {
        if (debugMode) console.log("The piece", whichPiece, "has changed. Property:", whichProperty, "to value", whichValue)
        board[whichPiece][whichProperty] = whichValue;
    }

    function initPlayers() {
        player1 = {
            hasWin: false,
            isItsTurn: false,
            isSelecting: false,
            isInGame: false,
            onPiece: -1,
            //To store the first piece the player was on
            firstPiece: -1,
            //Player arrived to the other side of the board?
            crossedBoard: false,
            color: "#990099"
        }

        player2 = {
            hasWin: false,
            isItsTurn: false,
            isSelecting: false,
            //Game has started but is player on the board?
            isInGame: false,
            onPiece: -1,
            firstPiece: -1,
            crossedBoard: false,
            color: "#009900"
        }
    }

    function initBoard() {
        var pieceTypes = [1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 4, 4, 4, 4];
        shuffle(pieceTypes);
        if (debugMode) console.log(pieceTypes);

        for (var i=0; i<16; i++) {
            board[i] = {
                //type 0 none, 1 arrow1, 2 arrow2, 3 none, 4 arrow4
                type: pieceTypes.shift(),

                //state 0 down, 1 up
                state: 0,

                //rotation 0 up, 1 right (90º), 2 down (180º),3 left
                rotation: Math.floor(Math.random() * 10) % 4,

                //isActive = selectable when player#.isSelecting = true
                isActive: true,

                //Player 0 no one, 1 player1, 2 player2. Which player is on the piece
                player: 0
            };
        }
    }

    /**
     * Shuffles array in place.
     * @param {Array} a items An array containing the items.
     *
     * https://stackoverflow.com/questions/6274339/how-can-i-shuffle-an-array
     */
    function shuffle(a) {
        var j, x, i;
        for (i = a.length - 1; i > 0; i--) {
            j = Math.floor(Math.random() * (i + 1));
            x = a[i];
            a[i] = a[j];
            a[j] = x;
        }

        return a;
    }
}
