import QtQuick 2.9
import Ubuntu.Components 1.3

Item {
    property string winner
    property string playerColor

    Rectangle {
        id: bg
        anchors.centerIn: banner
        width: banner.width
        height: banner.height * 5
        color: theme.palette.normal.background
        opacity: 0.7
    }

    Rectangle {
        id: banner
        width: parent.width
        height: mainText.height + units.gu(2)
        color: playerColor
        opacity: 0.3
    }

    Label {
        id: mainText
        text: i18n.tr("%1 wins!").arg(winner)
        width: parent.width
        horizontalAlignment: Text.AlignHCenter
        anchors.centerIn: banner
        textSize: Label.Large
        font.bold: true
    }

    Button {
        anchors {
            top: banner.bottom
            topMargin: units.gu(4)
            horizontalCenter: parent.horizontalCenter
        }

        text: i18n.tr("Play again")
        color: UbuntuColors.blue

        onClicked: {
            root.initPlayers();
            root.initBoard();
            mainStack.clear()
            mainStack.push(Qt.resolvedUrl("PageBoard.qml"))
        }
    }
}
