/*
 * Copyright (C) 2019  Joan CiberSheep
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ubuntu-calculator-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Ubuntu.Components 1.3

Page {
    id: aboutPage

    anchors.fill: parent

    header: PageHeader {
        id: aboutHeader
        title: i18n.tr('About')
        flickable: mainflick
    }

    property string iconAppRute: "../../assets/logo.svg"
    property string source: "<a href='https://gitlab.com/cibersheep/q-turn-for-ubuntu-touch'>Gitlab</a>"
    property string license: "<a href='https://www.gnu.org/licenses/gpl-2.0.html'>GPLv2</a>"
    property string qturnLicense: i18n.tr("Free for personal use")
    property string darkColor: theme.palette.normal.backgroundText
    property string selectionColor: "#33888888"

    ListView {
        id: mainflick
        anchors.fill: parent
        section.property: "category"
        section.criteria: ViewSection.FullString

        section.delegate: ListItemHeader {
            title: section
        }

        header: Item {
            width: parent.width
            height: iconTop.height + rulesInfo.height + units.gu(26)

            UbuntuShape {
                id: iconTop
                width: units.gu(20)
                height: width
                aspect: UbuntuShape.Flat

                anchors{
                    horizontalCenter: parent.horizontalCenter
                    top: parent.top
                    topMargin: units.gu(6)
                }

                source: Image {
                    sourceSize.width: parent.width
                    sourceSize.height: parent.height
                    source: iconAppRute
                }
            }

            Label {
                id: appNameLabel
                anchors.top: iconTop.bottom
                anchors.topMargin: units.gu(4)
                width: parent.width
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                text: i18n.tr("Q-turn App")
                font.bold: true
            }

            Label {
                id: appInfo
                anchors.top: appNameLabel.bottom
                anchors.topMargin: units.gu(2)
                width: parent.width
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                text: i18n.tr("Version %1. Source %2").arg(Qt.application.version).arg(source)
                onLinkActivated: Qt.openUrlExternally(link)
                linkColor: darkColor
            }

            Label {
                id: licenseInfo
                anchors.top: appInfo.bottom
                anchors.topMargin: units.gu(2)
                width: parent.width
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                text: i18n.tr("Under License %1").arg(license)
                onLinkActivated: Qt.openUrlExternally(link)
                linkColor: darkColor
            }

            Label {
                id: licenseQturn
                anchors.top: licenseInfo.bottom
                anchors.topMargin: units.gu(1)
                width: parent.width
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                text: i18n.tr("Q-Turn License %1").arg("<a href='http://wunderland.com/LooneyLabs/Qturn/'>" + qturnLicense + "</a>")
                onLinkActivated: Qt.openUrlExternally(link)
                linkColor: darkColor
            }

            Column {
                id: rulesInfo
                width: parent.width - units.gu(4)
                spacing: units.gu(2)

                anchors{
                    top: licenseQturn.bottom
                    topMargin: units.gu(4)
                    horizontalCenter: parent.horizontalCenter
                }

                Label {
                    width: parent.width
                    wrapMode: Text.AlignHCenter
                    horizontalAlignment: Text.AlignJustify
                    text: i18n.tr("<b>Rules:</b> Q-Turn is a quick and unpredictable little game played on a dynamic board made up of 16 disks.")
                }


                Label {
                    width: parent.width
                    wrapMode: Text.AlignHCenter
                    horizontalAlignment: Text.AlignJustify
                    text: i18n.tr("The goal is to get your token to the opposite corner and back again first. The disks indicate in which directions you may move. Landing on certain spaces lets you rotate one or all of the disks, changing the potential paths across the board, and since you aren't allowed to pass, fate will sometimes send you heading off in the wrong direction.")
                }

                Label {
                    width: parent.width
                    wrapMode: Text.AlignHCenter
                    horizontalAlignment: Text.AlignJustify
                    text: i18n.tr("<b>Start:</b> The baldest player goes first. (If there aren't any bald player, pick the person with the shortest or least amount of hair.)")
                }

                Label {
                    width: parent.width
                    wrapMode: Text.AlignHCenter
                    horizontalAlignment: Text.AlignJustify
                     /*and rotate it to the position of your choice*/
                    text: i18n.tr("<b>Taking a Turn:</b> On your first turn, simply move your token onto your starting corner. Each time you move your token onto a disk that is still face down, it will flip over. You then set your token down on the disk and take whatever action may apply, as on a normal turn.")
                }

                Label {
                    width: parent.width
                    wrapMode: Text.AlignHCenter
                    horizontalAlignment: Text.AlignJustify
                    text: i18n.tr("On subsequent turns, you may rotate in place (if you are on a One Way arrow) or you may move your token one space, following one of the arrows on the disk you occupy, and take the Special Action, if any (see next section). You may not pass. You may not move diagonally and you may not move off the board.")
                }

                Label {
                    width: parent.width
                    wrapMode: Text.AlignHCenter
                    horizontalAlignment: Text.AlignJustify
                    text: i18n.tr("<b>Special Actions:</b>")
                }

                PieceAbout {
                    icon: "../../assets/arrow4.svg"
                }

                Label {
                    width: parent.width
                    wrapMode: Text.AlignHCenter
                    horizontalAlignment: Text.AlignJustify
                    text: i18n.tr("The Q-Turner: Whenever you land on a disk with arrows pointing in all four directions, you MUST then immediately rotate all revealed disks a quarter turn counter-clockwise.")
                }

                PieceAbout {
                    icon: "../../assets/arrow2.svg"
                }

                Label {
                    width: parent.width
                    wrapMode: Text.AlignHCenter
                    horizontalAlignment: Text.AlignJustify
                    text: i18n.tr("The Double Arrow: Whenever you land on this type of disk, you MAY also then rotate any one disk (other than your own) a quarter turn counter-clockwise.")
                }

                PieceAbout {
                    icon: "../../assets/arrow1.svg"
                }

                Label {
                    width: parent.width
                    wrapMode: Text.AlignHCenter
                    horizontalAlignment: Text.AlignJustify
                    text: i18n.tr("The One Way Arrow: No action is taken when you land on this type of disk. However, if you are on such a space at the beginning of your turn, you may choose to re-orient that disk INSTEAD of moving your token to the indicated space. You may re-orient the disk however you wish.")
                }

                Label {
                    width: parent.width
                    wrapMode: Text.AlignHCenter
                    horizontalAlignment: Text.AlignJustify
                    text: i18n.tr("<b>Bouncing:</b> Each space can hold only one token. If someone occupies a space you could potentially enter, you may choose to bounce off their piece and remain where you are. This allows you to re-use the special action associated with your space, just as though you had landed on it for the first time.")
                }

                Label {
                    width: parent.width
                    wrapMode: Text.AlignHCenter
                    horizontalAlignment: Text.AlignJustify
                    text: i18n.tr("You may only bounce off of an occupied space on the board; you may not bounce against the edge of the board.")
                }

                UbuntuShape {
                    width: units.gu(18)
                    height: width
                    aspect: UbuntuShape.Flat

                    anchors.horizontalCenter: parent.horizontalCenter

                    source: Image {
                        sourceSize.width: parent.width
                        sourceSize.height: parent.height
                        source: "../../assets/win.svg"
                    }
                }

                Label {
                    width: units.gu(24)
                    wrapMode: Text.AlignHCenter
                    horizontalAlignment: Text.AlignHCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: i18n.tr("Example of one player's path to victory")
                }

                Label {
                    width: parent.width
                    wrapMode: Text.AlignHCenter
                    horizontalAlignment: Text.AlignJustify
                    text: i18n.tr("<b>Winning:</b> The object of the game is to move your token from the disk you started on across the board to the disk in the opposite corner, and back again. As soon as someone completes that journey and moves his or her token back onto their starting disk, the game ends and that player wins.")
                }

                Label {
                    width: parent.width
                    wrapMode: Text.AlignHCenter
                    horizontalAlignment: Text.AlignJustify
                    text: i18n.tr("<b>Strategies:</b> It's very important to plan ahead when deciding how to orient a disk. If you anticipate that the other player will land on a Q-Turner next, you will want to compensate for this rotation by pointing your disk a clockwise turn away from the direction you really want to move.")
                }

                Label {
                    width: parent.width
                    wrapMode: Text.AlignHCenter
                    horizontalAlignment: Text.AlignJustify
                    text: i18n.tr("The most direct path across the board would be along the diagonal, but since you can't do that, you frequently have to decide whether to point to the left or the right. Usually it's better to go to the right ­ that way, you'll still be able to make progress across the board, even it someone hits a Q-Turner and the disk ends up pointing to the left.")
                }
            }
        }

        model: gameStoriesModel

        delegate: ListItem {
            height: storiesDelegateLayout.height
            divider.visible: false
            highlightColor: selectionColor

            ListItemLayout {
                id: storiesDelegateLayout
                title.text: mainText
                subtitle.text: secondaryText
                ProgressionSlot { name: link !== "" ? "next" : ""}
            }

            onClicked: model.link !== "" ? Qt.openUrlExternally(model.link) : null
        }

        ListModel {
            id: gameStoriesModel

            Component.onCompleted: initialize()

            function initialize() {
                gameStoriesModel.append({ category: i18n.tr("App Development"), mainText: "Joan CiberSheep",
                 secondaryText: "", link: "https://cibersheep.com/" })

                gameStoriesModel.append({ category: i18n.tr("Q-Turn Game Credits"), mainText: "Andrew Looney",
                 secondaryText: i18n.tr("Game design"), link: "https://www.looneylabs.com/" })
                gameStoriesModel.append({ category: i18n.tr("Q-Turn Game Credits"), mainText: "Kristin Looney",
                 secondaryText: i18n.tr("Game producer"), link: "http://wunderland.com/WTS/Kristin/Kristin.html" })

            }
        }
    }
}
