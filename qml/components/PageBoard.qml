import QtQuick 2.9
import Ubuntu.Components 1.3

Page {
    id: boardPage
    anchors.fill: parent

    property bool showBanner: false
    property var allPieces: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]

    signal gameStep
    signal playerChanged
    signal selectInsteadOfMove
    signal hideSelecButton
    signal notRotating
    signal showOkButton(bool showIt)

    header: PageHeader {
        id: boardHeader
        title: i18n.tr('Q-Turn')

        trailingActionBar {

            actions: Action {
                id: actionInfo
                iconName: "info"
                shortcut: "Ctrl+i"
                text: i18n.tr("Information")
                onTriggered: mainStack.push(Qt.resolvedUrl("PageAbout.qml"));
            }
        }
    }

    Grid {
        width: Math.min(parent.width, parent.height - boardHeader.height) - units.gu(2)
        height: width
        columns: 4

        anchors {
            centerIn: parent
            verticalCenterOffset: boardHeader.height / 2
        }

        Repeater {
            id: actualBoard
            model: 16

            Piece {
                id: thisPiece

                isActive: board[index].isActive
                playerToken: board[index].player
                stateToken: board[index].state
                typeToken: board[index].type
                orientToken: 90 * board[index].rotation
                previousOrientToken: orientToken + 90

                width: parent.width / 4
                height: width
            }
        }
    }

    //TODO: Move indicators and buttons to the sides if in landscape

    PlayerTurnIndicator {
        id: player1TurnIndicator
        color: player1.color

        anchors {
            bottom: root.isLandscape
                ? undefined
                : parent.bottom
            right: root.isLandscape
                ? parent.right
                : undefined
            horizontalCenter: root.isLandscape
                ? undefined
                : parent.horizontalCenter
        }
    }

    PlayerTurnIndicator {
        id: player2TurnIndicator
        color: player2.color

        anchors {
            top: root.isLandscape
                ? undefined
                : boardHeader.bottom
            left: root.isLandscape
                ? undefined
                : parent.left
        }
    }

    RotateButton {
        id: player1Selection

        anchors {
            right: isLandscape
                ? parent.right
                : undefined
            rightMargin: isLandscape
                ? units.gu(2)
                : undefined

            bottom: isLandscape
                ? undefined
                : parent.bottom
            bottomMargin: isLandscape
                ? undefined
                : units.gu(2)
        }


        onClicked: boardPage.selectInsteadOfMove();
        onEndRound: boardPage.gameStep();
    }

    RotateButton {
        id: player2Selection
        rotation: 180

        anchors {
            left: isLandscape
                ? parent.left
                : undefined
            leftMargin: isLandscape
                ? units.gu(2)
                : undefined

            top: isLandscape
                ? undefined
                : boardHeader.bottom
            topMargin: isLandscape
                ? undefined
                : units.gu(2)
        }

        onClicked: boardPage.selectInsteadOfMove();
        onEndRound: boardPage.gameStep();
    }

    Loader {
        active: showBanner
        anchors.centerIn: parent
        width: parent.width
        sourceComponent: winMessage

        Component {
            id: winMessage

            WinnMessage {
                winner: player1.hasWin
                    ? i18n.tr("Player 1")
                    : i18n.tr("Player 2")
                playerColor: player1.hasWin
                    ? player1.color
                    : player2.color
            }
        }
    }

    Component.onCompleted: {
        //Start the game mechanics
        gameStep();
    }

    onGameStep: {
        if (debugMode) console.log("DEBUG: Game Step!");

        if (!player1.isItsTurn && !player2.isItsTurn) {
            if (debugMode) console.log("DEBUG: No one has turn. Let's start game");
            //The game has just started. Let player1 start
            player1.isItsTurn = true;
            playerChanged();
            return;
        }

        //Check if someone has win
        if (player1.hasWin || player2.hasWin) {
            if (debugMode) console.log("DEBUG: Player 1 has win", player1.hasWin, "Player 2 has win", player2.hasWin)
            //Deactivate whole board
            boardPage.changeIsActive(allPieces, false);

            //Hide buttons
            player1Selection.visible = false;
            player2Selection.visible = false;
            //Show Win status
            showBanner = true;

            return;
        }

        //Update selection mode and change player turn
        if (player1.isSelecting || player2.isSelecting) {
            if (debugMode) console.log("DEBUG: One player was in selecting mode. Getting out");
            player1.isSelecting = false;
            player2.isSelecting = false;
            notRotating();
        }

        boardPage.changeIsActive(allPieces, true);
        player1.isItsTurn = !player1.isItsTurn;
        player2.isItsTurn = !player2.isItsTurn;
        playerChanged();
    }

    onPlayerChanged: {
        if (debugMode) console.log("DEBUG: Player has changed!") //, "\n", JSON.stringify(player1), "\n", JSON.stringify(player2));

        player1TurnIndicator.visible = player1.isItsTurn
        player2TurnIndicator.visible = player2.isItsTurn

        if (player1.onPiece !== -1) {
            player1Selection.visible = player1.isItsTurn && actualBoard.itemAt(player1.onPiece).typeToken == 1;
            player1Selection.text = i18n.tr("Rotate current disk")
            player1Selection.textEnd = i18n.tr("End round")
            boardPage.showOkButton(false);
        }

        if (player2.onPiece !== -1) {
            player2Selection.visible = player2.isItsTurn && actualBoard.itemAt(player2.onPiece).typeToken == 1;
            player2Selection.text = i18n.tr("Rotate current disk")
            player2Selection.textEnd = i18n.tr("End round")
            boardPage.showOkButton(false);
        }

        //Show to the correct player possible pieces to interact
        if (player1.isItsTurn) {
            showPieceToPlayer(player1, [12,15], player1Selection);
        } else {
            showPieceToPlayer(player2, [0,3], player2Selection);
        }
    }

    function showPieceToPlayer(plyr, initialPieces,buttonsToShow) {
        if (debugMode) console.log("DEBUG:",plyr,"turn");

        //Is the current player not started game?
        if (!plyr.isInGame) {
            boardPage.changeIsActive(allPieces, false);
            boardPage.changeIsActive(initialPieces, true);
            plyr.isInGame = true;
        } else {
            if (debugMode) console.log("DEBUG:",plyr,".onPiece",plyr.onPiece)
            boardPage.changeIsActiveForPosiblePieceToMove(plyr.onPiece, true);
        }
    }

    //Rotate piece 1 instead of moving
    onSelectInsteadOfMove: {
        boardPage.changeIsActive(allPieces, false);

        if (player1.isInGame && player1.isItsTurn) {
            switch(board[player1.onPiece].type) {
                case 1:
                    boardPage.changeIsActive([player1.onPiece], true);
                    player1.isSelecting = true;
                    break;
                case 2:
                    boardPage.changeIsActiveForVisiblePiece(true);
                    boardPage.changeIsActive([player1.onPiece], false);
                    player1.isSelecting = true;
                    break;
                default:
                    console.log("DEBUG: Error. onSelectInsteadOfMove but piece type is",board[player1.onPiece].type);
            }

            hideSelecButton();
        } else if (player2.isInGame && player2.isItsTurn) {
            switch(board[player2.onPiece].type) {
                case 1:
                    boardPage.changeIsActive([player2.onPiece], true);
                    player2.isSelecting = true;
                    break;
                case 2:
                    boardPage.changeIsActiveForVisiblePiece(true);
                    boardPage.changeIsActive([player2.onPiece], false);
                    player2.isSelecting = true;
                    break;
                default:
                    console.log("DEBUG: Error. onSelectInsteadOfMove but piece type is",board[player1.onPiece].type);
            }

            hideSelecButton();
        } else {
            console.log("DEBUG: Error. onSelectInsteadOfMove but none players are inGame nor inTurn")
        }
    }

    function returnAdjacentPiece(origenPiece, direction) {
        var adjacentPiece = -1;

        switch(direction) {
            case "top":
                if (origenPiece > 3) {
                    return origenPiece -4;
                }
                break;
            case "right":
                if (origenPiece %4 !== 3) {
                    return origenPiece +1;
                }
                break;
            case "bottom":
                if (origenPiece < 12) {
                    return origenPiece +4;
                }
                break;
            case "left":
                if (origenPiece %4 !== 0) {
                    return origenPiece -1;
                }
                break;
            default:
                console.log("Debug: Attention, could define rotation for piece",origenPiece,"rotation",board[origenPiece].rotation,"direction",direction)
        }

        return adjacentPiece;
    }

    function changeIsActive(listOfPieces, boolValue) {
        for (var i=0; i<listOfPieces.length; i++) {
            board[listOfPieces[i]].isActive = boolValue;
            actualBoard.itemAt(listOfPieces[i]).isActive = boolValue;
        };
    }

    function changeIsActiveForVisiblePiece(boolValue) {
        changeIsActive(allPieces, false);

        var listToProcess = [];

        for (var i=0; i<16; i++) {
            if (board[i].state > 0) {
                listToProcess.push(i);
            }
        };

        if (debugMode) console.log("Debug: changeIsActiveForVisible", listToProcess);
        changeIsActive(listToProcess, boolValue);
    }

    function changeIsActiveForPosiblePieceToMove(pieceToMoveFrom, boolValue) {
        changeIsActive(allPieces, false);

        var listToProcess = [];
        var facing = ["top","right","bottom","left"];
        var numberOfDirections = 1;

        switch(board[pieceToMoveFrom].type) {
            case 0:
            case 3:
                console.log("DEBUG: Error. Type can't be",board[pieceToMoveFrom].type);
                break;
            case 1:
                facing = [facing[board[pieceToMoveFrom].rotation]];
                break;
            case 2:
                if (board[pieceToMoveFrom].rotation == 0 || board[pieceToMoveFrom].rotation == 2) {
                    facing = ["top","bottom"];
                } else {
                    facing = ["right","left"];
                }
                numberOfDirections = 2;
                break;
            case 4:
                numberOfDirections = 4;
                break;
            default:
                console.log("Debug: Error. No type")
        }

        if (debugMode) console.log("Debug: changeIsActiveForPosiblePieceToMove", numberOfDirections,facing);

        for (var i=0; i<numberOfDirections; i++) {
            var adjacent = returnAdjacentPiece(pieceToMoveFrom, facing[i]);

            if (adjacent !== -1) {
                listToProcess.push(adjacent);
                if (debugMode) console.log("DEBUG: listToProcess, adjacent",listToProcess,adjacent)
            } else {
                console.log("Debug: error. Returned adjacent piece is",adjacent)
            }
        }

        if (debugMode) console.log("Debug: changeIsActiveForPosiblePieceToMove", listToProcess);
        changeIsActive(listToProcess, boolValue);
    }

    function rotatePiece(piece) {
        if (board[piece].state > 0) {
            actualBoard.itemAt(piece).previousOrientToken = 90 * board[piece].rotation;
            //(4 + X -1) % 4 always stays in range: 0-1 -> 3
            board[piece].rotation = (4 + board[piece].rotation -1) % 4;
            actualBoard.itemAt(piece).orientToken = 90 * board[piece].rotation;

            if (debugMode) console.log("DEBUG: Piece", piece, "rotated", board[piece].rotation);
        }
    }

    function rotateVisiblePieces() {
        for (var i=0; i<16; i++) {
            rotatePiece(i)
        };
    }
}
