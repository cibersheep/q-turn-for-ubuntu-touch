import QtQuick 2.9
import Ubuntu.Components 1.3

Item {
    id: item
    visible: false
    property alias text: button.text
    property alias textEnd: buttonOk.text
    property bool isRotating: false
    property bool showButton: false

    signal clicked
    signal endRound

    width: isLandscape
        ? icon.width
        : parent.width
    height: isLandscape
        ? icon.height
        : button.height

    anchors {
        verticalCenter: isLandscape
            ? parent.verticalCenter
            : undefined
        verticalCenterOffset: isLandscape
            ? boardHeader.height / 2
            : 0
        horizontalCenter: isLandscape
            ? undefined
            : parent.horizontalCenter
    }

    Button {
        id: buttonOk
        visible: parent.visible && !isLandscape && (isRotating || showButton)

        anchors {
            horizontalCenter: parent.horizontalCenter
        }

        onClicked: item.endRound();
    }

    Button {
        id: button
        visible: parent.visible && !isLandscape && !isRotating

        anchors {
            horizontalCenter: parent.horizontalCenter
            bottom: buttonOk.top
            bottomMargin: units.gu(2)
        }

        onClicked: item.clicked();
    }

    Column {
        id: icon
        visible: parent.visible && isLandscape
        spacing: units.gu(2)

        Icon {

            visible: parent.visible && (isRotating || showButton)
            width: units.gu(3)
            height: width
            name: "tick"

            MouseArea {
                anchors.fill: parent

                onClicked: item.endRound();
            }
        }

        Icon {

            visible: parent.visible && !isRotating
            width: units.gu(3)
            height: width
            source: "../../assets/rotate.svg"

            MouseArea {
                anchors.fill: parent

                onClicked: item.clicked();
            }
        }
    }

    Connections {
        target: boardPage

        onHideSelecButton: isRotating = true;

        onNotRotating: isRotating = false;

        onShowOkButton: showButton = showIt;
    }
}
