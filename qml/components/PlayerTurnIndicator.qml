import QtQuick 2.9
import Ubuntu.Components 1.3

Rectangle {
    visible: false
    anchors.verticalCenter: root.isLandscape
        ? parent.verticalCenter
        : undefined
    width: root.isLandscape
        ? units.gu(1)
        : parent.width
    height: root.isLandscape
        ? parent.height
        : units.gu(1)
    opacity: 0.6
}
