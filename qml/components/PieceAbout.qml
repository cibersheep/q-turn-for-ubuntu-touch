import QtQuick 2.9
import Ubuntu.Components 1.3

Rectangle {
    property alias icon: image.source
    width: units.gu(5)
    height: width
    radius: width * 0.5
    color: theme.palette.normal.background
    anchors.horizontalCenter: parent.horizontalCenter

    border {
        width: units.dp(2)
        color: theme.palette.normal.foregroundText
    }

    UbuntuShape {
        width: units.gu(4)
        height: width
        aspect: UbuntuShape.Flat

        anchors.centerIn: parent

        source: Image {
            id: image
            sourceSize.width: parent.width
            sourceSize.height: parent.height
        }
    }
}
