import QtQuick 2.9
import Ubuntu.Components 1.3


Item {
    signal rotateToken()

    property int playerToken
    property int stateToken
    property int typeToken
    property int orientToken
    property int previousOrientToken
    property bool isActive

    onPlayerTokenChanged: root.pieceChanged(index, "player", playerToken);

    onStateTokenChanged:  root.pieceChanged(index, "state", stateToken);

    onIsActiveChanged:    root.pieceChanged(index, "isActive", isActive);

    onOrientTokenChanged: {
        if (debugMode) console.log("DEBUG: ORIENTATION.",index, "orientToken has changed", orientToken, "board", board[index].rotation)
        rotateToken();
    }

    Rectangle {
        radius: width * 0.5

        color: stateToken > 0
            ? theme.palette.normal.background
            : theme.palette.normal.foreground

        opacity: isActive
            ? 1
            : 0.4

        anchors {
            fill: parent
            margins: units.gu(.5)
        }

        border {
            width: units.dp(2)
            color: stateToken > 0
                ? theme.palette.normal.foregroundText
                : theme.palette.normal.backgroundSecondaryText
        }

        Image {
            id: pieceSimbol
            anchors {
                fill: parent
                margins: units.gu(1.5)
            }

            asynchronous: true
            visible: stateToken > 0
            source: "../../assets/arrow" + typeToken.toString() + ".svg"
            sourceSize.width: parent.width > 32
                ? units.gu(64)
                : units.gu(16)
            sourceSize.height: width
        }

        RotationAnimation on rotation {
            id: anim
            from: previousOrientToken
            to: previousOrientToken - 90
            duration: 250
        }
    }

    Rectangle {
        id: token
        visible: playerToken > 0
        radius: width * 0.5
        width: parent.width / 3
        height: width
        anchors.centerIn: parent
        opacity: 0.6

        color: playerToken == 1
            ? player1.color
            : player2.color
    }

    MouseArea {
        anchors.fill: parent
        enabled: isActive

        onClicked: clickedOnPiece()
    }

    onRotateToken: {
        anim.start();
    }

    function clickedOnPiece() {
        //Have we clicked on a covered piece?
        //TODO: Player chooses orientation
        if (stateToken < 1) {
            stateToken = 1;
        }

        if (player1.isItsTurn) {
            if (player1.isSelecting) {
                if (debugMode) console.log("DEBUG: We might want to rotate instead of anything else");
                if (board[player1.onPiece].type == 2) {
                    boardPage.rotatePiece(index);
                    gameStep();
                    return;
                } else if (board[player1.onPiece].type == 1) {
                    boardPage.rotatePiece(index);
                } else {
                    console.log("DEGUB: Player 1 isSelecting but no piece type")
                }

                return;
            }

            //Don't delete player token if both are in the same tile
            if (player1.onPiece !== -1) {
                actualBoard.itemAt(player1.onPiece).playerToken == 2
                    ? actualBoard.itemAt(player1.onPiece).playerToken = 2
                    : actualBoard.itemAt(player1.onPiece).playerToken = 0;
            } else {
                //Is the first piece the player is on
                player1.firstPiece = index;
            }

            //Check if the other player is in that positon
            if (playerToken > 0) {
                if (debugMode) console.log("DEBUG: P1,",player1.onPiece,index)
                boardPage.changeIsActive(boardPage.allPieces, false);
                boardPage.changeIsActive([player1.onPiece], true);
                return;
            } else {
                playerToken = 1;
            }

            //Update player position
            player1.onPiece = index;

            //Check if player arrived to the opposite corner
            if (index == 0 && player1.firstPiece == 15) {
                player1.crossedBoard = true
            }

            if (index == 3 && player1.firstPiece == 12) {
                player1.crossedBoard = true
            }

            if (index == player1.firstPiece && player1.crossedBoard == true) {
                player1.hasWin = true
            }

            //Check if on a <-> piece and wait for Rotate or Finish round
            if (typeToken == 2) {
                //player1.isSelecting = true;

                //Show buttons to finish round
                player1Selection.visible = true;
                boardPage.showOkButton(true);
                player1Selection.text = i18n.tr("Rotate disks");
                player1Selection.textEnd = i18n.tr("End round")

                return;
            }

        } else {
            if (player2.isSelecting) {
                if (debugMode) console.log("DEBUG: We might want to rotate instead of anything else")

                if (board[player2.onPiece].type == 2) {
                    boardPage.rotatePiece(index);
                    gameStep();
                    return;
                } else if (board[player2.onPiece].type == 1) {
                    boardPage.rotatePiece(index);
                } else {
                    console.log("DEGUB: Player 1 isSelecting but no piece type")
                }

                return;
            }

            if (player2.onPiece !== -1) {
                actualBoard.itemAt(player2.onPiece).playerToken == 1
                    ? actualBoard.itemAt(player2.onPiece).playerToken = 1
                    : actualBoard.itemAt(player2.onPiece).playerToken = 0;
            } else {
                //Is the first piece the player is on
                player2.firstPiece = index;
            }

            //Check if the other player is in that positon
            if (playerToken > 0) {
                boardPage.changeIsActive(boardPage.allPieces, false);
                boardPage.changeIsActive([player2.onPiece], true);
                return;
            } else {
                playerToken = 2;
            }

            player2.onPiece = index;

            //Check if player arrived to the opposite corner
            if (index == 12 && player2.firstPiece == 3) {
                player2.crossedBoard = true
            }

            if (index == 15 && player2.firstPiece == 0) {
                player2.crossedBoard = true
            }

            if (index == player2.firstPiece && player2.crossedBoard == true) {
                player2.hasWin = true
            }

            //Check if on a <-> piece and wait for Rotate or Finish round
            if (typeToken == 2) {
                //player2.isSelecting = true;

                //Show buttons to finish round
                player2Selection.visible = true;
                boardPage.showOkButton(true);
                player2Selection.text = i18n.tr("Rotate disks");
                player2Selection.textEnd = i18n.tr("End round")

                return;
            }
        }

        //Piece is 4 directions?
        if (typeToken == 4) {
            boardPage.rotateVisiblePieces();
        }

        //Are we in selection mode? -> rotate and leave selection mode

        //We are not in selection mode? -> move



        //Send a signal to the board
        boardPage.gameStep();
    }
}
