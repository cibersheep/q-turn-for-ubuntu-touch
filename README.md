# Qturn
[![OpenStore](https://open-store.io/badges/ca.svg)](https://open-store.io/app/qturn.cibersheep)

Quick and unpredictable little game played on a dynamic board

## License

Copyright (C) 2019  Joan CiberSheep

Licensed under the MIT license

Q-Turn is published under [free for personal use agreement](http://wunderland.com/LooneyLabs/Qturn/)

Q-Turn is:
Designed by Andrew Looney
Produced by Kristin Looney
Published by Looney Laboratories, Inc. 

Buy Q-Turn at http://www.looneylabs.com/
